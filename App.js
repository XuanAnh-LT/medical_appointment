
var expanded = true;

function setStayFocus1(){
  document.getElementById("btntype1").style.backgroundColor = "#009201";
  document.getElementById("btntype1").style.color = "#fff";
  document.getElementById("btntype2").style.backgroundColor = "#fff";
  document.getElementById("btntype2").style.color = "#000";
  document.getElementById("btntype3").style.backgroundColor = "#fff";
  document.getElementById("btntype3").style.color = "#000";
}

function setStayFocus2(){
  document.getElementById("btntype2").style.backgroundColor = "#009201";
  document.getElementById("btntype2").style.color = "#fff";
  document.getElementById("btntype1").style.backgroundColor = "#fff";
  document.getElementById("btntype1").style.color = "#000";
  document.getElementById("btntype3").style.backgroundColor = "#fff";
  document.getElementById("btntype3").style.color = "#000";
}

function setStayFocus3(){
  document.getElementById("btntype3").style.backgroundColor = "#009201";
  document.getElementById("btntype3").style.color = "#fff";
  document.getElementById("btntype2").style.backgroundColor = "#009201";
  document.getElementById("btntype2").style.color = "#fff";
  document.getElementById("btntype2").style.backgroundColor = "#fff";
  document.getElementById("btntype2").style.color = "#000";
}

function showCheckboxes ()  {
  expanded = !expanded;
  var array = ["hohap", "timmach", "sanphu", "noitiet", "tieuhoa"];
  var temp_option = document.getElementById('temp_option');
  temp_option.style.display = 'none';

  for (var i = 0; i < array.length; i++) {
    // expanded = false;
    var checkboxes = document.getElementById(array[i]);

    if (!expanded) {
      checkboxes.style.display = "block";
      checkboxes.style.overflowY = "scroll";
      checkboxes.style.height = "200px";

    } else {
      checkboxes.style.display = "none";
    }
  }
}



var turnToPickdate = () => {
  document.getElementById("date_picker").focus();
}

var turnToPickhour = () => {
  if (document.getElementById("date_picker").value === "") {
    document.getElementById("hour_register_without_date").style.display = "block";
    document.getElementById("hour_register_within_date").style.display = "none";
  }
  else {
    document.getElementById("hour_register_without_date").style.display = "none";
    document.getElementById("hour_register_within_date").style.display = "block";
  }

  document.getElementById("hour_register_within_date").focus();
}

var focusHour1 = () => {
  document.getElementById("hour1").style.backgroundColor = "#009201";
  document.getElementById("hour1").style.color = "#fff";
  document.getElementById("hour2").style.backgroundColor = "#fff";
  document.getElementById("hour2").style.color = "#000";
  document.getElementById("hour3").style.backgroundColor = "#fff";
  document.getElementById("hour3").style.color = "#000";
  document.getElementById("hour4").style.backgroundColor = "#fff";
  document.getElementById("hour4").style.color = "#000";
}

var focusHour2 = () => {
  document.getElementById("hour1").style.backgroundColor = "#fff";
  document.getElementById("hour1").style.color = "#000";
  document.getElementById("hour2").style.backgroundColor = "#009201";
  document.getElementById("hour2").style.color = "#fff";
  document.getElementById("hour3").style.backgroundColor = "#fff";
  document.getElementById("hour3").style.color = "#000";
  document.getElementById("hour4").style.backgroundColor = "#fff";
  document.getElementById("hour4").style.color = "#000";
}

var focusHour3 = () => {
  document.getElementById("hour1").style.backgroundColor = "#fff";
  document.getElementById("hour1").style.color = "#000";
  document.getElementById("hour2").style.backgroundColor = "#fff";
  document.getElementById("hour2").style.color = "#000";
  document.getElementById("hour3").style.backgroundColor = "#009201";
  document.getElementById("hour3").style.color = "#fff";
  document.getElementById("hour4").style.backgroundColor = "#fff";
  document.getElementById("hour4").style.color = "#000";
}

var focusHour4 = () => {
  document.getElementById("hour1").style.backgroundColor = "#fff";
  document.getElementById("hour1").style.color = "#000";
  document.getElementById("hour2").style.backgroundColor = "#fff";
  document.getElementById("hour2").style.color = "#000";
  document.getElementById("hour3").style.backgroundColor = "#fff";
  document.getElementById("hour3").style.color = "#000";
  document.getElementById("hour4").style.backgroundColor = "#009201";
  document.getElementById("hour4").style.color = "#fff";
}



var nextToPatient = () => {
  if (document.getElementById("date_picker").value === "" || document.getElementById("description_detail").value === "") {
    if (document.getElementById("date_picker").value === "") {
      // setMessageDate("Hãy chọn ngày khám!");
      document.getElementById("messageDate").style.display = "block";
      document.getElementById("messageDate").textContent="Hãy chọn ngày khám";
    } else{
      document.getElementById("messageDate").style.display = "none";
    }

    if (document.getElementById("description_detail").value === "") {
      // error_description[0] = "Hãy nhập mô tả!";
      // setMessageDescription("Hãy nhập mô tả!");
      document.getElementById("messageDescription").style.display = "block";
      document.getElementById("messageDescription").textContent="Hãy nhập mô tả";
    } else{
      document.getElementById("messageDescription").style.display = "none";
    }
  } 
  else {
    document.getElementById("Register").style.display = "none";
    document.getElementById("PatientInfor").style.display = "block";
    document.getElementById("messageDate").style.display = "none";
    document.getElementById("messageDescription").style.display = "none";

    // countTotalFee();
  }
}

var backToRegister = () => {
  document.getElementById("Register").style.display = "block";
  document.getElementById("PatientInfor").style.display = "none";
}

var nextToSecondpage = () => {
  if (document.getElementById("patient_name").value === "" || document.getElementById("patient_dob").value === "" ||
    document.getElementById("patient_phone") === "" || document.getElementById("patient_gender").value === "auto" ||
    document.getElementById("patient_address").value === "") {

      if (document.getElementById("patient_name").value === "") {
        document.getElementById("messageName").style.display = "block";
        document.getElementById("messageName").textContent="Hãy nhập họ và tên";
      } else{
        document.getElementById("messageName").style.display = "none";
      }
    
      if (document.getElementById("patient_dob").value === "") {
        document.getElementById("messageDob").style.display = "block";
        document.getElementById("messageDob").textContent="Hãy chọn ngày sinh";
      } else{
        document.getElementById("messageDob").style.display = "none";
      }

    if (document.getElementById("patient_phone").value === "") {
        document.getElementById("messagePhone").style.display = "block";
        document.getElementById("messagePhone").textContent="Hãy nhập số điện thoại";
      } else{
        document.getElementById("messagePhone").style.display = "none";
    }

    if (document.getElementById("patient_gender").value === "auto") {
        document.getElementById("messageGender").style.display = "block";
        document.getElementById("messageGender").textContent="Hãy chọn giới tính";
      } else{
        document.getElementById("messageGender").style.display = "none";
    }

    if (document.getElementById("patient_address").value === "") {
        document.getElementById("messageAddress").style.display = "block";
        document.getElementById("messageAddress").textContent="Hãy nhập địa chỉ";
      } else{
        document.getElementById("messageAddress").style.display = "none";
    }
  }
  else {
    // alert("đã ấn chuyển qua secondpage");
    document.getElementById("secondpage").style.display = "block";
    document.getElementById("firstpage").style.display = "none";
    // setPatientName(document.getElementById("patient_name").value);
  }
}

var backToFirstpage = () => {
  document.getElementById("firstpage").style.display = "block";
  document.getElementById("secondpage").style.display = "none";
  document.getElementById("Register").style.display = "block";
  document.getElementById("PatientInfor").style.display = "none";
}

function countFee1() {
  let count1 = 0;
  let select1 = document.getElementsByName("select1");
  for (let i = 0; i < select1.length; i++) {
    if (select1[i].checked) {
      console.log(select1[i]);
      count1 = parseFloat(select1[i].value);
    }
  }
  return count1;
}

function countFee2() {
  let count2 = 0;
  let select2 = document.getElementsByName("select2");
  for (let i = 0; i < select2.length; i++) {
    if (select2[i].checked) {
      console.log(select2[i]);
      count2 = parseFloat(select2[i].value);
    }
  }
  return count2;
}

function countFee3() {
  let count3 = 0;
  let select3 = document.getElementsByName("select3");
  for (let i = 0; i < select3.length; i++) {
    if (select3[i].checked) {
      console.log(select3[i]);
      count3 = parseFloat(select3[i].value);
    }
  }
  return count3;
}

function countFee4() {
  let count4 = 0;
  let select4 = document.getElementsByName("select4");
  for (let i = 0; i < select4.length; i++) {
    if (select4[i].checked) {
      console.log(select4[i]);
      count4 = parseFloat(select4[i].value);
    }
  }
  return count4;
}

function countFee5() {
  let count5 = 0;
  let select5 = document.getElementsByName("select5");
  for (let i = 0; i < select5.length; i++) {
    if (select5[i].checked) {
      console.log(select5[i]);
      count5 = parseFloat(select5[i].value);
    }
  }
  return count5;
}

var countTotalFee = () => {
  let count1 = countFee1();
  let count2 = countFee2();
  let count3 = countFee3();
  let count4 = countFee4();
  let count5 = countFee5();


  let totalFee = count1 + count2 + count3 + count4 + count5;

  console.log(totalFee);
  document.getElementById("totalFee").textContent = totalFee + ".000đ";

}


